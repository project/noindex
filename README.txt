MODULE TITLE
-------------
Noindex Module


PROJECT PAGE
-------------
https://www.drupal.org/project/noindex


SHORT DESCRIPTION
------------------
This module adds the noindex robots-meta-tag to the HTML head of nodes
in order to prevent their content from being indexed by search engines.


REQUIREMENTS
------------
None.


INSTALLATION
-------------
Please visit the project page for detailed installation instructions and
troubleshooting advice.


QUICK INSTALLATION
------------------
1. Extract all files.
2. Edit the file called 'noindex.module'.
3. Upload the complete 'noindex' folder to your '/modules' directory.
4. Login to your website.
5. Enable the module on the 'Extend' page (e.g. 'yourdomain.com/admin/modules').
6. Click 'Install' at the bottom of the page.


FEEDBACK AND SUPPORT
---------------------
Please use the issue queue on the project page to report any issues you encounter.


Enjoy!
